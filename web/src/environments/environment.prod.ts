export const environment = {
  production: true,
  baseUrl: "http://localhost:16000/api/",
  imgUrl: "http://localhost:16000/uploads/"
};
