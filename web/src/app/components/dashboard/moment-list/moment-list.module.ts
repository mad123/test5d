import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { MomentListRoutingModule } from './moment-list-routing.module';
import { MomentListComponent } from './moment-list/moment-list.component';


@NgModule({
  declarations: [MomentListComponent],
  imports: [
    CommonModule,
    MomentListRoutingModule,
    PaginationModule
  ]
})
export class MomentListModule { }
