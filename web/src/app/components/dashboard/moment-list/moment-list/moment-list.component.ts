import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { MomentService } from 'src/app/services/moments/moment.service';
import { environment } from "../../../../../environments/environment.prod";

@Component({
  selector: 'app-moment-list',
  templateUrl: './moment-list.component.html',
  styleUrls: ['./moment-list.component.css']
})
export class MomentListComponent implements OnInit {
  modalRef: BsModalRef;
  momentList: any;
  momentId: any
  moment: any
  imgPath: any
  userId: any

  startItem: any;
  endItem: any;
  returnedArray: any;
  momentLength: any;
  itemsPerPage: any;

  constructor(private modalService: BsModalService, private momentService: MomentService, private toster: ToastrService) { }

  deleteModal(template: TemplateRef<any>, id) {
    this.modalRef = this.modalService.show(template);
    this.momentId = id;
  }

  ngOnInit(): void {
    var user = JSON.parse(localStorage.getItem("user"))
    this.userId = user._id;

    this.momentLists(this.userId, '');
  }

  pageChanged(event: PageChangedEvent): void {
    this.startItem = (event.page - 1) * event.itemsPerPage;
    this.endItem = event.page * event.itemsPerPage;
    this.returnedArray = this.momentList.slice(this.startItem, this.endItem);
  }

  momentLists(userId, id) {
    this.momentService.getMoment(userId, id).subscribe(res => {
      this.momentList = res;
      this.imgPath = environment.imgUrl;

      this.momentLength = this.momentList.length;

      this.itemsPerPage = 5;
      const startItem = this.startItem == undefined ? '0' : this.startItem;
      const endItem = this.endItem == undefined ? '5' : this.endItem;
      this.returnedArray = this.momentList.slice(startItem, endItem);
    })
  }

  deleteUser() {
    this.momentService.deleteMoment(this.momentId).subscribe(res => {
      this.toster.error(res['msg']);
      this.modalRef.hide();
      this.momentLists(this.userId, '');
    })
  }

}
