import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddMomentRoutingModule } from './add-moment-routing.module';
import { AddMomentComponent } from './add-moment/add-moment.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AddMomentComponent],
  imports: [
    CommonModule,
    AddMomentRoutingModule,
    ReactiveFormsModule
  ]
})
export class AddMomentModule { }
