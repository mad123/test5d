import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MomentService } from 'src/app/services/moments/moment.service';

@Component({
  selector: 'app-add-moment',
  templateUrl: './add-moment.component.html',
  styleUrls: ['./add-moment.component.css']
})
export class AddMomentComponent implements OnInit {
  momentForm: FormGroup;
  valid = false;
  fileData: any;
  momentById: any;
  hiddenImg: any;
  imgPath: any;
  userId: any;

  constructor(private formBuilder: FormBuilder, private momentService: MomentService, private toster: ToastrService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    var user = JSON.parse(localStorage.getItem("user"))
    this.userId = user._id;

    if(this.route.snapshot.params.id) {
      this.momentLists();
    }
    this.momentForm = this.formBuilder.group({
      title: ['', Validators.required],
      tags: ['', Validators.required],
      image: ['', Validators.required],
      user_id: ['', Validators.required]
    })
  }

  get f() { return this.momentForm.controls; }

  addMonent() {
    this.valid = true;
    this.momentForm.patchValue({ user_id: this.userId })
    if(this.momentForm.invalid) {
      return;
    } else {
      this.momentService.addMoment(this.momentForm.value, this.route.snapshot.params.id).subscribe(res => {
        if(res['success'] == true) {
          this.toster.success(res['msg']);
          this.router.navigate(['/moments']);

          if(!this.route.snapshot.params.id || (this.momentForm.value.image != this.momentById.image)) {
            var formData = new FormData;
            formData.append('mimg', this.fileData, this.momentForm.value.image);
            this.momentService.upload(formData).subscribe(res => { })
          }
        } else {
          this.toster.error(res['msg']);
        }     
      })
    }
  }

  getImageData(e) {
    this.valid = false;
    this.fileData = e.target.files[0];
    this.hiddenImg = new Date().toISOString() + e.target.files[0].name;
    this.momentForm.patchValue({ image: this.hiddenImg, user_id: this.userId })
  }

  momentLists() {
    this.momentService.getMoment(this.userId, this.route.snapshot.params.id).subscribe(res => {
      this.momentById = res[0];
    })
  }
}
