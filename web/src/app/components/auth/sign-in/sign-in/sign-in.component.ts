import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  signInForm: FormGroup;
  valid = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private toster: ToastrService, private router: Router) { }

  ngOnInit(): void {
    this.signInForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  get f() { return this.signInForm.controls; }

  signIn() {
    this.valid = true;

    if(this.signInForm.invalid) {
      return;
    } else {
      this.authService.signIn(this.signInForm.value).subscribe(res => {
        console.log('res', res);
        if(res['success'] == true) {
          this.toster.success(res['msg']);
          this.router.navigate(['/moments']);
          localStorage.setItem("token", res['token']);
          localStorage.setItem("user", JSON.stringify(res['user']));
        } else {
          this.toster.error(res['msg']);
        }
      })
    }
  }
}
