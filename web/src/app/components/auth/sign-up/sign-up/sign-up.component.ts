import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  valid = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private toster: ToastrService, private router: Router) { }

  ngOnInit(): void {
    this.signUpForm = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.minLength(3)]],
      last_name: ['', [Validators.required, Validators.minLength(3)]],
      mobile_no: ['', [Validators.required, Validators.minLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      city: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  get f() { return this.signUpForm.controls; }

  signUp() {
    this.valid = true;

    if(this.signUpForm.invalid) {
      return;
    } else {
      this.authService.signUp(this.signUpForm.value).subscribe(res => {
        console.log('res', res);
        if(res['success'] == true) {
          this.toster.success(res['msg']);
          this.router.navigate(['/signin']);
        } else {
          this.toster.error(res['msg']);
        }
      })
    }
  }
}
