import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userName: any;

  constructor(private router: Router) { 
    
  }

  ngOnInit(): void {
    var user = JSON.parse(localStorage.getItem("user"))
    this.userName = user.first_name
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/signin']);
  }

}
