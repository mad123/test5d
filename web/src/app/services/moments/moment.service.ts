import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class MomentService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  addMoment(data, id) {
    var url = id ? this.baseUrl + 'add/moment/' + id : this.baseUrl + 'add/moment';
    return this.http.post(url, data);
  }

  upload(data) {
    return this.http.post(this.baseUrl + 'upload', data);
  }

  getMoment(userId, id) {
    var url = id == '' ? this.baseUrl + 'moment/' + userId : this.baseUrl + 'moment/' + id + '/' + userId;
    return this.http.get(url);
  }

  deleteMoment(id) {
    return this.http.get(this.baseUrl + 'delete/' + id);
  }
}
