import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  signUp(data) {
    return this.http.post(this.baseUrl + 'signup', data);
  }
  signIn(data) {
    return this.http.post(this.baseUrl + 'signin', data);
  }

  checkLogin() {
    const token = localStorage.getItem("token");
     if(!token ) {
       return false
     }
    return true;
  }
}
