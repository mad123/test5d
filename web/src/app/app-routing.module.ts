import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from "./services/helper/auth-guard.guard";
import { SharedComponent } from "./components/layout/shared/shared.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'signin' },
  { path: 'signin', loadChildren: () => import('./components/auth/sign-in/sign-in.module').then(m => m.SignInModule) },
  { path: 'signup', loadChildren: () => import('./components/auth/sign-up/sign-up.module').then(m => m.SignUpModule) },

  { path: '', canActivate: [AuthGuardGuard], component: SharedComponent, children:[
    { path: 'moments', loadChildren: () => import('./components/dashboard/moment-list/moment-list.module').then(m => m.MomentListModule) },
    { path: 'add/moment/:id', loadChildren: () => import('./components/dashboard/add-moment/add-moment.module').then(m => m.AddMomentModule) }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
