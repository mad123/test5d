const express = require("express");
const mongoose = require("mongoose");
const Moment = mongoose.model("Moment");
var multer  = require('multer');
const router = express.Router();

// ======= upload img file into local storage start ===========
const moveImgFile = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, './public/uploads');
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname);
    }
  });

  var imgFile = multer({ storage: moveImgFile }).any('mimg');
  router.post('/upload', (req, res) => {
    imgFile(req, res, (err) => {
      if (err) {
        return res.end("Error uploading file.");
      }
      return res.json({ msg: "uploaded" });
    });
  });
// ======= upload img file into local storage end ============

router.post("/add/moment/:id?", (req, res) => {
    const { user_id, title, tags, image } = req.body;

    if(!title || !tags || !image) {
        res.json({ error: "all fields required" });
    } else {
        if(req.params.id != 'undefined' && req.params.id) {
            Moment.updateOne({ _id: req.params.id },{ $set: { user_id, title, tags, image } }).then(up => {
                res.json({ success: true, msg: "Moment updated" })
            }).catch(err => { console.log("err", err); })
        } else {
            Moment.findOne({ title }).then(result => {
                if(result) {
                    res.json({ success: false, msg: "Moment exits" });
                } else {
                    const moment = new Moment({ user_id, title, tags, image });
                    moment.save().then(save => {
                        res.json({ success: true, msg: "Moment Added" });
                    })
                }
            }).catch(err => { console.log("err", err); })
        }
    }
});

router.get("/moment/:id?/:user_id", (req, res) => {

    // console.log('req.params.id', req.params.id);
    // console.log('req.params.user_id', req.params.user_id);

    var findData = req.params.id ? Moment.find({_id: req.params.id }) : Moment.find({user_id: req.params.user_id });

    // console.log('findData', findData);
    
    findData.then(data => {
        res.json(data);
    }).catch(err => { console.log("err", err); })
});

// router.get('/del/:id', (req, res) => {
//     console.log('req', req.params);
// })

router.get('/delete/:id', (req, res) => {
    console.log('delete', req.params);
    Moment.remove({_id: req.params.id}).then(del => {
        if(del) {
            return res.json({msg: "Moment deleted"});
        }
    }).catch(err => { console.log('err', err); })
});

module.exports = router;