const express = require("express");
const mongoose = require("mongoose");
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const User = mongoose.model("User");
const { jwt_secret } = require("../config/key");

const router = express.Router();

router.post("/signup/:id?", (req, res) => {
    const { first_name, last_name, mobile_no, email, city, password } = req.body;

    if(!first_name || !last_name || !mobile_no || !email || !city || !password) {
        res.json({ error: "all fields required" });
    } else {
        if(req.params.id != 'undefined' && req.params.id) {
            User.updateOne({ _id: req.params.id },{ $set: { first_name, last_name, mobile_no, email, city } }).then(up => {
                res.json({ sucess: true, msg: "user updated" })
            }).catch(err => { console.log("err", err); })
        } else {
            User.findOne({ email }).then(result => {
                if(result) {
                    res.json({ success: false, msg: "user exits" });
                } else {
                    bcrypt.hash(password, 8).then(password => {
                        const user = new User({ first_name, last_name, mobile_no, email, city, password });
        
                        user.save().then(save => {
                            res.json({ success: true, msg: "User Added" });
                        })
                    })
                }
            }).catch(err => { console.log("err", err); })
        }

    }
});

router.post("/signin", (req, res) => {
    const { email, password } = req.body;

    if(!email || !password) {
        res.json({ error: "All fields are required" });
    } else {
        User.findOne({ email }).then(result => {
            if(!result) {
                res.json({ success: false, msg: "invalid credential" });
            } else {
                bcrypt.compare(password, result.password).then(doMatch => {
                    if(!doMatch) {
                        res.json({ success: false, msg: "invalid credential" });
                    } else {
                        var token = jwt.sign({_id: result._id }, jwt_secret);
                        var {_id, first_name, email } = result;
                        res.json({ success: true, msg: "user loggged in", token, user: {_id, first_name, email } });
                    }
                })
            }
        }).catch(err => { console.log("err",  err); })
    }
});

router.get("/user/:id?", (req, res) => {
    var findData = req.params.id ? User.find({_id: req.params.id }) : User.find();
    
    findData.then(data => {
        res.json(data);
    }).catch(err => { console.log("err", err); })
});

router.get('/user/delete/:id', (req, res) => {
    User.remove({_id: req.params.id}).then(del => {
        if(del) {
            return res.json({msg: "user deleted"});
        }
    }).catch(err => { console.log('err', err); })
});

module.exports = router;