const mongoose = require("mongoose");
const { url } = require("./key");

const option = { useNewUrlParser: true, useUnifiedTopology: true };

mongoose.connect(url, option, (err) => {
    if(err) throw err;
    console.log('database connected');
})