const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var path = require('path');
const app = express();
const port = 16000;

require("./config/db");
require("./models/user");
require("./models/moment");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

app.use("/api", require("./routes/auth"));
app.use("/api", require("./routes/moments"));

app.listen(port, () => {
    console.log('server listen on port', port);
});
