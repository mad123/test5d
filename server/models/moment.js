const mongoose = require("mongoose");

const momentSchema = mongoose.Schema({
    user_id: { type: String, require: true },
    title: { type: String, require },
    tags: { type: String, require },
    image: { type: String, require },
    created : { type : Date, default: Date.now }
});

mongoose.model("Moment", momentSchema);