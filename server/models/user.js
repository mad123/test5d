const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
    first_name: { type: String, require },
    last_name: { type: String, require },
    mobile_no: { type: String, require },
    email: { type: String, require },
    city: { type: String, require },
    password: { type: String, require },
    created : { type : Date, default: Date.now }
});

mongoose.model("User", userSchema);